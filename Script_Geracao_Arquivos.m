clear, clc;

LFCR = sprintf('\n');

% true: demora mais, por�m gera arquivos menores
% false: mais r�pido, por�m gera arquivos maiores
custoso = true;

if( exist('Files', 'dir') == 0 )
    mkdir('Files');
end

%% Carrega os dados
if( exist('Variables/Treinamento_aumentado.mat', 'file') > 0 && exist('Variables/Validacao_aumentado.mat', 'file') > 0 && exist('Variables/Teste.mat', 'file') > 0 && ...
    exist('Variables/Treinamento_diminuido.mat', 'file') > 0 && exist('Variables/Validacao_diminuido.mat', 'file') )

load('Variables/Treinamento_aumentado.mat');
load('Variables/Treinamento_diminuido.mat');
load('Variables/Validacao_aumentado.mat');
load('Variables/Validacao_diminuido.mat');
load('Variables/Teste.mat');

%% Salva os dados do conjunto de teste
filePtr = fopen('Files/Teste.txt', 'w');

if(filePtr ~= 0)
    if(custoso)

        for i = 1 : size(setTest, 1)
            for j = 1 : size(setTest, 2)
                if(setTest(i,j) == 1.0 || setTest(i,j) == 0.0)
                    fprintf(filePtr, ' %d', setTest(i,j));
                else
                    fprintf(filePtr, ' %0.9f', setTest(i,j));
                end
            end
            fprintf(filePtr, '%s', LFCR);
        end

    else
        save('Files/Teste.txt', 'setTest',  '-ascii', '-double');
    end
end

fclose(filePtr);

%% Salva os dados do conjunto de Treinamento_aumentado
filePtr = fopen('Files/Treinamento_aumentado.txt', 'w');

if(filePtr ~= 0)
    if(custoso)

        for i = 1 : size(shuffledSetTrain_aumentado, 1)
           for j = 1 : size(shuffledSetTrain_aumentado, 2)
               if(shuffledSetTrain_aumentado(i,j) == 1.0 || shuffledSetTrain_aumentado(i,j) == 0.0)
                   fprintf(filePtr, ' %d', shuffledSetTrain_aumentado(i,j));
               else
                   fprintf(filePtr, ' %0.9f', shuffledSetTrain_aumentado(i,j));
               end
           end
           fprintf(filePtr, '%s', LFCR);
        end

    else
        save('Files/Treinamento_aumentado.txt', 'shuffledSetTrain_aumentado', '-ascii', '-double');
    end
end

fclose(filePtr);

%% Salva os dados do conjunto de Treinamento_diminuido
filePtr = fopen('Files/Treinamento_diminuido.txt', 'w');

if(filePtr ~= 0)
    if(custoso)

        for i = 1 : size(shuffledSetTrain_diminuido, 1)
           for j = 1 : size(shuffledSetTrain_diminuido, 2)
               if(shuffledSetTrain_diminuido(i,j) == 1.0 || shuffledSetTrain_diminuido(i,j) == 0.0)
                   fprintf(filePtr, ' %d', shuffledSetTrain_diminuido(i,j));
               else
                   fprintf(filePtr, ' %0.9f', shuffledSetTrain_diminuido(i,j));
               end
           end
           fprintf(filePtr, '%s', LFCR);
        end

    else
        save('Files/Treinamento_diminuido.txt', 'shuffledSetTrain_diminuido', '-ascii', '-double');
    end
end

fclose(filePtr);

%% Salva os dados do conjunto de valida��o_aumentado
filePtr = fopen('Files/Validacao_aumentado.txt', 'w');

if(filePtr ~= 0)
    if(custoso)

        for i = 1 : size(setValidation_aumentado, 1)
           for j = 1 : size(setValidation_aumentado, 2)
               if(setValidation_aumentado(i,j) == 1.0 || setValidation_aumentado(i,j) == 0.0)
                   fprintf(filePtr, ' %d', setValidation_aumentado(i,j));
               else
                   fprintf(filePtr, ' %0.9f', setValidation_aumentado(i,j));
               end
           end
           fprintf(filePtr, '%s', LFCR);
        end

    else
        save('Files/Validacao_aumentado.txt', 'setValidation_aumentado', '-ascii', '-double');
    end
end

fclose(filePtr);

%% Salva os dados do conjunto de valida��o_diminuido
filePtr = fopen('Files/Validacao_diminuido.txt', 'w');

if(filePtr ~= 0)
    if(custoso)

        for i = 1 : size(setValidation_diminuido, 1)
           for j = 1 : size(setValidation_diminuido, 2)
               if(setValidation_diminuido(i,j) == 1.0 || setValidation_diminuido(i,j) == 0.0)
                   fprintf(filePtr, ' %d', setValidation_diminuido(i,j));
               else
                   fprintf(filePtr, ' %0.9f', setValidation_diminuido(i,j));
               end
           end
           fprintf(filePtr, '%s', LFCR);
        end

    else
        save('Files/Validacao_diminuido.txt', 'setValidation_diminuido', '-ascii', '-double');
    end
end

fclose(filePtr);

end