%% Limpando console
clear, clc

%% Lendo os dados
numColumns = 49;
dir = 'PAKDD-PAKDD_GERMANO.cod';

filePtr = fopen(dir, 'r');
fgetl(filePtr);
data = fscanf(filePtr,'%f');                                    %Um vetor coluna que contem os dados escaneados   
data = reshape(data, numColumns, length(data)/numColumns)';     %Transforma em matriz

dontBuy = data( data(:, numColumns) == 1, : );                  %Seleciona a linha toda se o ultimo valor for 1 ou 0
buy     = data( data(:, numColumns) == 0, : );

fclose(filePtr);

if( exist('Variables', 'dir') == 0 )
    mkdir('Variables');
end
save('Variables/Data.mat', 'data');
save('Variables/Class.mat', 'dontBuy', 'buy');

clear ans dir filePtr;

%% Calculo da quantidade de dados para o conjunto
% Primeiro: 25% para teste       = 175 de buy e 10.000 de dontBuy
% Segundo: 50% para treinamento  = 350 de buy e 20.000 de dontBuy
% Terceiro: 25% para valida��o   = 175 de buy e 10.000 de dontBuy

buy_size_row      = size(buy, 1);
dontBuy_size_rows = size(dontBuy, 1);

test_size_buy     = floor( buy_size_row * 0.25 );
test_size_dontBuy = floor( dontBuy_size_rows * 0.25 );

train_size_buy     = floor( buy_size_row * 0.5 );
train_size_dontBuy = floor( dontBuy_size_rows * 0.5 );

%% Divis�o dos dados de Teste
rows = randperm(buy_size_row, test_size_buy);       %Cria um vetor de tamanho test_size_buy com valores de 1 at� buy_size_row
buy_test = buy(rows, :);
buy(rows, :) = [];
buy_size_row = buy_size_row - test_size_buy;

rows = randperm(dontBuy_size_rows, test_size_dontBuy);
dontBuy_test = dontBuy(rows, :);
dontBuy(rows, :) = [];
dontBuy_size_rows = dontBuy_size_rows - test_size_dontBuy;

%% Divis�o dos dados de Treinamento
rows = randperm(buy_size_row, train_size_buy);
buy_train = buy(rows, :);
buy(rows, :) = [];

rows = randperm(dontBuy_size_rows, train_size_dontBuy);
dontBuy_train = dontBuy(rows, :);
dontBuy(rows, :) = [];

%% Divis�o dos dados de Valida��o
buy_validation = buy;
dontBuy_validation = dontBuy;

%% Limpando dados indesejados
clear buy buy_size_row dontBuy dontBuy_size_rows rows test_size_buy ...
    test_size_dontBuy train_size_buy train_size_dontBuy;

%% Iguala quantidade de dados para o conjunto de treinamento e valida��o, aumentando a classe minorit�ria
size_buy_train = size(buy_train, 1);
size_dontBuy_train = size(dontBuy_train, 1);

size_buy_validation = size(buy_validation, 1);
size_dontBuy_validation = size(dontBuy_validation, 1);

buy_train_aumentado =  buy_train(mod(randperm(size_dontBuy_train), size_buy_train)+1, :);
buy_validation_aumentado = buy_validation(mod(randperm(size_dontBuy_validation), size_buy_validation)+1, :);

%% Iguala quantidade de dados para o conjunto de treinamento e valida��o, diminuindo a classe majorit�ria
dontBuy_train_diminuido = dontBuy_train(randperm(size_dontBuy_train, size_buy_train), :);
dontBuy_validation_diminuido = dontBuy_validation(randperm(size_dontBuy_validation, size_buy_validation), :);

%% Constroi cada conjunto
setTest = [buy_test; dontBuy_test];
setTrain_aumentado = [buy_train_aumentado; dontBuy_train];
setTrain_diminuido = [buy_train; dontBuy_train_diminuido];
setValidation_aumentado = [buy_validation_aumentado; dontBuy_validation];
setValidation_diminuido = [buy_validation; dontBuy_validation_diminuido];

setTest = setTest(:, 2:end);
setTrain_aumentado = setTrain_aumentado(:, 2:end);
setTrain_diminuido = setTrain_diminuido(:, 2:end);
setValidation_aumentado = setValidation_aumentado(:, 2:end);
setValidation_diminuido = setValidation_diminuido(:, 2:end);

%% Embaralha o conjunto de treinamento
shuffledSetTrain_aumentado = setTrain_aumentado(randperm(size(setTrain_aumentado,1)), :);
shuffledSetTrain_diminuido = setTrain_diminuido(randperm(size(setTrain_diminuido,1)), :);

%% Salva os dados em formato de vari�veis
save('Variables/Treinamento_aumentado.mat', 'shuffledSetTrain_aumentado');
save('Variables/Treinamento_diminuido.mat', 'shuffledSetTrain_diminuido');
save('Variables/Validacao_aumentado.mat', 'setValidation_aumentado');
save('Variables/Validacao_diminuido.mat', 'setValidation_diminuido');
save('Variables/Teste.mat', 'setTest');
