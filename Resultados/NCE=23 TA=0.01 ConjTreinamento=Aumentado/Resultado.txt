Configuracao da rede 
Numero de neuronios na camada escondinda: 23 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 1000000 
Taxa de aprendizado: 0.010 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.20073 
MSE para o conjunto de validacao: 0.22101 
MSE para o conjunto de teste: 0.19923 

Area sob a curva roc 
Neuronio da classe compradora: 0.684 
Neuronio da classe nao compradora: 0.671