Configuracao da rede 
Numero de neuronios na camada escondinda: 8 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 50000 
Taxa de aprendizado: 0.100 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.20676 
MSE para o conjunto de validacao: 0.22438 
MSE para o conjunto de teste: 0.20174 

Area sob a curva roc 
Neuronio da classe compradora: 0.683 
Neuronio da classe nao compradora: 0.682