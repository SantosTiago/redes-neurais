Configuracao da rede 
Numero de neuronios na camada escondinda: 4 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 50000 
Taxa de aprendizado: 0.500 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.21425 
MSE para o conjunto de validacao: 0.21577 
MSE para o conjunto de teste: 0.20723 

Area sob a curva roc 
Neuronio da classe compradora: 0.684 
Neuronio da classe nao compradora: 0.689