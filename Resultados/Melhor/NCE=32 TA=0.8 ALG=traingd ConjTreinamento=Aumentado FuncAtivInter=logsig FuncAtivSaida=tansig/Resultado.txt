Configuracao da rede 
Numero de neuronios na camada escondinda: 32 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 100000 
Taxa de aprendizado: 0.800 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.37915 
MSE para o conjunto de validacao: 0.37716 
MSE para o conjunto de teste: 0.24216 

Area sob a curva roc 
Neuronio da classe compradora: 0.548 
Neuronio da classe nao compradora: 0.569 

Matriz de Confusao 
                                                      Classes Verdadeiras 

                                             Classe Compradora 	 Classe nao compradora 
Classificacao       Classe Compradora               79          	     3008 
                    Classe nao compradora           96          	     6992 

Medidas extraidas da matriz de confusao 
fp Rate: 0.30 
tp Rate: 0.45 
Precision: 0.03 
Recall: 0.45 
Accuracy: 0.69 
F-measure: 0.05 
