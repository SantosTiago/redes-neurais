Configuracao da rede 
Numero de neuronios na camada escondinda: 32 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 100000 
Taxa de aprendizado: 0.800 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.24982 
MSE para o conjunto de validacao: 0.24994 
MSE para o conjunto de teste: 0.24984 

Area sob a curva roc 
Neuronio da classe compradora: 0.567 
Neuronio da classe nao compradora: 0.435 

Matriz de Confusao 
                                                      Classes Verdadeiras 

                                             Classe Compradora 	 Classe nao compradora 
Classificacao       Classe Compradora              150          	     7611 
                    Classe nao compradora           25          	     2389 

Medidas extraidas da matriz de confusao 
fp Rate: 0.76 
tp Rate: 0.86 
Precision: 0.02 
Recall: 0.86 
Accuracy: 0.25 
F-measure: 0.04 
