Configuracao da rede 
Numero de neuronios na camada escondinda: 16 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 1000000 
Taxa de aprendizado: 0.010 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.19630 
MSE para o conjunto de validacao: 0.22300 
MSE para o conjunto de teste: 0.21008 

Area sob a curva roc 
Neuronio da classe compradora: 0.672 
Neuronio da classe nao compradora: 0.653