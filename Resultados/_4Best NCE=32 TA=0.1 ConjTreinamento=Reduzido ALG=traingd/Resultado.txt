Configuracao da rede 
Numero de neuronios na camada escondinda: 32 
Numero de padroes de treinamento: 700 
Maximo numero de iteracoes: 1000000 
Taxa de aprendizado: 0.100 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.20716 
MSE para o conjunto de validacao: 0.22836 
MSE para o conjunto de teste: 0.21145 

Area sob a curva roc 
Neuronio da classe compradora: 0.675 
Neuronio da classe nao compradora: 0.694