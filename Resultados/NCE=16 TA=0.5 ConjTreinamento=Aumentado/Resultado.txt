Configuracao da rede 
Numero de neuronios na camada escondinda: 16 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 50000 
Taxa de aprendizado: 0.500 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.21067 
MSE para o conjunto de validacao: 0.22060 
MSE para o conjunto de teste: 0.20668 

Area sob a curva roc 
Neuronio da classe compradora: 0.674 
Neuronio da classe nao compradora: 0.684