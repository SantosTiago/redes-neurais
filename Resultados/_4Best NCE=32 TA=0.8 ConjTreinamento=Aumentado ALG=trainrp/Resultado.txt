Configuracao da rede 
Numero de neuronios na camada escondinda: 32 
Numero de padroes de treinamento: 40000 
Maximo numero de iteracoes: 1000000 
Taxa de aprendizado: 0.800 
Criterio de minimo erro de treinamento: 0.000 
Criterio de quantidade maxima de falhas na validacao: 10 
Criterio de gradiente minimo: 0.000 

MSE 
MSE para o conjunto de treinamento: 0.18019 
MSE para o conjunto de validacao: 0.23812 
MSE para o conjunto de teste: 0.20553 

Area sob a curva roc 
Neuronio da classe compradora: 0.674 
Neuronio da classe nao compradora: 0.683