clear
clc
echo on

funcaoAtivacao = {'tansig', 'purelin', 'logsig'};

len = length(funcaoAtivacao);

for i = 1 : len

    for j = 1 : len


        FuncAtivInter = funcaoAtivacao{i};
        FuncAtivSaida = funcaoAtivacao{j};

        %    Informacoes sobre a rede e os dados
        numEntradas   = 46;    % Numero de nodos de entrada
        numEscondidos = 32;    % Numero de nodos escondidos
        numSaidas     = 2;     % Numero de nodos de saida
        numTr         = 40000; % Numero de padroes de treinamento
        numVal        = 20000; % Numero de padroes de validacao
        numTeste      = 10175; % Numero de padroes de teste

        echo off

        %    Abrindo arquivos 
        arquivoTreinamento = fopen('Files/Treinamento.txt','rt');
        arquivoValidacao   = fopen('Files/Validacao.txt','rt');
        arquivoTeste       = fopen('Files/Teste.txt','rt');

        %    Lendo arquivos e armazenando dados em matrizes
        dadosTreinamento    = fscanf(arquivoTreinamento,'%f',[(numEntradas + numSaidas), numTr]);   % Lendo arquivo de treinamento
        entradasTreinamento = dadosTreinamento(1:numEntradas, 1:numTr);
        saidasTreinamento   = dadosTreinamento((numEntradas + 1):(numEntradas + numSaidas), 1:numTr);       

        dadosValidacao      = fscanf(arquivoValidacao,'%f',[(numEntradas + numSaidas), numVal]);    % Mesmo processo para validacao
        entradasValidacao   = dadosValidacao(1:numEntradas, 1:numVal);
        saidasValidacao     = dadosValidacao((numEntradas + 1):(numEntradas + numSaidas), 1:numVal);

        dadosTeste          = fscanf(arquivoTeste,'%f',[(numEntradas + numSaidas), numTeste]);      % Mesmo processo para teste
        entradasTeste       = dadosTeste(1:numEntradas, 1:numTeste);
        saidasTeste         = dadosTeste((numEntradas + 1):(numEntradas + numSaidas), 1:numTeste);

        %    Fechando arquivos
        fclose(arquivoTreinamento);
        fclose(arquivoValidacao);
        fclose(arquivoTeste);

        %   Criando a rede (para ajuda, digite 'help newff')
        matrizFaixa = zeros([numEntradas, 2]);
        for entrada = 1 : numEntradas;  % Cria 'matrizFaixa', que possui 'numEntradas' linhas, cada uma sendo igual a [0 1].
             matrizFaixa(entrada,:) = [0 1];  
        end

        rede = newff(matrizFaixa,[numEscondidos numSaidas],{FuncAtivInter,FuncAtivSaida},'traingd','learngd','mse');
        % matrizFaixa                    : indica que todas as entradas possuem valores na faixa entre 0 e 1
        % [numEscondidos numSaidas]      : indica a quantidade de nodos escondidos e de saida da rede
        % {'logsig','logsig'}            : indica que os nodos das camadas escondida e de saida terao funcao de ativacao sigmoide logistica
        % 'traingdm','learngdm'          : indica que o treinamento vai ser feito com gradiente descendente (backpropagation)
        % 'mse'                          : indica que o erro a ser utilizado vai ser mse (erro quadr�trico m�dio)

        % Inicializa os pesos da rede criada (para ajuda, digite 'help init')
        rede = init(rede);

        %   Parametros para visualiza��o do estado da rede
        rede.trainParam.showCommandLine = true; % Habilita a visualiza��o da informa��o de treinamento atrav�s de GUI
        rede.trainParam.showWindow      = true; % Habilita a visualiza��o da informa��o de treinamento atrav�s de linha de comnado
        rede.trainParam.show            = 100;  % Iteracoes entre exibicoes na tela (preenchendo com 'NaN', nao exibe na tela)

        %   Parametros do treinamento (para ajuda, digite 'help traingd')
        rede.trainParam.epochs   = 1000000;% Maximo numero de iteracoes
        rede.trainParam.lr       = 0.8;    % Taxa de aprendizado
        rede.trainParam.goal     = 0;      % Criterio de minimo erro de treinamento
        rede.trainParam.max_fail = 10;     % Criterio de quantidade maxima de falhas na validacao
        rede.trainParam.min_grad = 0;      % Criterio de gradiente minimo
        rede.trainParam.time     = inf;    % Tempo maximo (em segundos) para o treinamento

        fprintf('\nTreinando ...\n')

        conjuntoValidacao.P = entradasValidacao; % Entradas da validacao
        conjuntoValidacao.T = saidasValidacao;   % Saidas desejadas da validacao

        %   Treinando a rede
        [redeNova,desempenho,saidasRede,erros] = train(rede,entradasTreinamento,saidasTreinamento,[],[],conjuntoValidacao);
        % redeNova   : rede apos treinamento
        % desempenho : apresenta os seguintes resultados
        %              desempenho.perf  - vetor com os erros de treinamento de todas as iteracoes (neste exemplo, escolheu-se erro mse)
        %              desempenho.vperf - vetor com os erros de validacao de todas as iteracoes (idem)
        %              desempenho.epoch - vetor com as iteracoes efetuadas
        % saidasRede : matriz contendo as saidas da rede para cada padrao de treinamento
        % erros      : matriz contendo os erros para cada padrao de treinamento
        %             (para cada padrao: erro = saida desejada - saida da rede)
        % Obs.       : Os dois argumentos de 'train' preenchidos com [] apenas sao utilizados quando se usam delays
        %             (para ajuda, digitar 'help train')

        fprintf('\nTestando ...\n\n');

        %    Testando a rede
        [saidasRedeTeste,~,~,errosTeste,desempenhoTeste] = sim(redeNova,entradasTeste,[],[],saidasTeste);
        % saidasRedeTeste : matriz contendo as saidas da rede para cada padrao de teste
        % errosTeste      : matriz contendo os erros para cada padrao de teste
        %                  (para cada padrao: erro = saida desejada - saida da rede)
        % desempenhoTeste : erro de teste (neste exemplo, escolheu-se erro mse)

        fprintf('MSE para o conjunto de treinamento: %6.5f \n',desempenho.perf(length(desempenho.perf)));
        fprintf('MSE para o conjunto de validacao: %6.5f \n',desempenho.vperf(length(desempenho.vperf)));
        fprintf('MSE para o conjunto de teste: %6.5f \n',desempenhoTeste);

        %     Calculando o erro de classificacao para o conjunto de teste
        %     (A regra de classificacao e' winner-takes-all, ou seja, o nodo de saida que gerar o maior valor de saida
        %      corresponde a clamse do padrao).
        %     Obs.: Emse erro so' faz sentido se o problema for de classificacao. Para problemas que nao sao de classificacao,
        %           emse trecho do script deve ser eliminado.
        
        [maiorSaidaRede, nodoVencedorRede] = max (saidasRedeTeste);
        [maiorSaidaDesejada, nodoVencedorDesejado] = max (saidasTeste);

        %      Obs.: O comando 'max' aplicado a uma matriz gera dois vetores: um contendo os maiores elementos de cada coluna
        %            e outro contendo as linhas onde ocorreram os maiores elementos de cada coluna.

        classificacoesErradas = 0;

        for padrao = 1 : numTeste;
            if nodoVencedorRede(padrao) ~= nodoVencedorDesejado(padrao),
                classificacoesErradas = classificacoesErradas + 1;
            end
        end

        erroClassifTeste = 100 * (classificacoesErradas/numTeste);

        fprintf('Erro de classificacao para o conjunto de teste: %6.5f\n',erroClassifTeste);

        % Calculo da curva ROC de cada neur�nio respons�vel por cada classe
        [FPR_neuronio_Buy,TPR_neuronio_Buy,~,AUC_neuronio_Buy] = perfcurve(nodoVencedorDesejado, saidasRedeTeste(1,:), 1, 'NegClass', 2);
        disp(['Area sob a curva roc do neuronio que classifica a classe de quem comprou: ', num2str(AUC_neuronio_Buy)]);

        [FPR_neuronio_dontBuy,TPR_neuronio_dontBuy,~,AUC_neuronio_dontBuy] = perfcurve(nodoVencedorDesejado, saidasRedeTeste(2,:), 2, 'NegClass', 1);
        disp(['Area sob a curva roc do neuronio que classifica a classe de quem nao comprou: ', num2str(AUC_neuronio_dontBuy)]);

        figure, plot(FPR_neuronio_Buy, TPR_neuronio_Buy), xlabel('Taxa de Falsos Positivos'), ylabel('Taxa de Verdadeiros Positivos'), title(['Curva ROC - Classe Compradora - Area = ', num2str(AUC_neuronio_Buy)]), grid on;
        plot_buy = gcf;
        figure, plot(FPR_neuronio_dontBuy, TPR_neuronio_dontBuy), xlabel('Taxa de Falsos Positivos'), ylabel('Taxa de Verdadeiros Positivos'), title(['Curva ROC - Classe Nao Compradora - Area = ', num2str(AUC_neuronio_dontBuy)]), grid on;
        plot_dontBuy = gcf;

        % Salvando os resultados
        OutFolderName = ['Resultados/Melhor/NCE=', int2str(numEscondidos), ' TA=', num2str(rede.trainParam.lr), ' ALG=traingd', ' ConjTreinamento=Aumentado', ' FuncAtivInter=', FuncAtivInter, ' FuncAtivSaida=', FuncAtivSaida];
        if( exist(OutFolderName, 'dir') == 0 )
            mkdir(OutFolderName);
        end

        matrizConfusao = zeros(2);
        for k = 1 : numTeste;
           matrizConfusao(nodoVencedorRede(k), nodoVencedorDesejado(k)) = matrizConfusao(nodoVencedorRede(k), nodoVencedorDesejado(k)) + 1;
        end

        TP = matrizConfusao(1,1);
        FP = matrizConfusao(1,2);
        FN = matrizConfusao(2,1);
        TN = matrizConfusao(2,2);

        P = TP + FN;
        N = FP + TN;

        fpRate = FP/N;
        tpRate = TP/P;
        precision = TP/(TP+FP);
        recall = TP/P;
        accuracy = (TP+TN)/(P+N);
        FMeasure = 2/( (1/precision) + (1/recall) );


        fptr = fopen([OutFolderName,'/Resultado.txt'], 'w');

        fprintf(fptr, 'Configuracao da rede \n');
        fprintf(fptr, 'Numero de neuronios na camada escondinda: %d \n', numEscondidos);
        fprintf(fptr, 'Numero de padroes de treinamento: %d \n', numTr);
        fprintf(fptr, 'Maximo numero de iteracoes: %d \n', rede.trainParam.epochs);
        fprintf(fptr, 'Taxa de aprendizado: %1.3f \n', rede.trainParam.lr);
        fprintf(fptr, 'Criterio de minimo erro de treinamento: %1.3f \n', rede.trainParam.goal);
        fprintf(fptr, 'Criterio de quantidade maxima de falhas na validacao: %d \n', rede.trainParam.max_fail);
        fprintf(fptr, 'Criterio de gradiente minimo: %1.3f \n', rede.trainParam.min_grad);
        fprintf(fptr, '\n');
        fprintf(fptr, 'MSE \n');
        fprintf(fptr, 'MSE para o conjunto de treinamento: %6.5f \n',desempenho.perf(length(desempenho.perf)));
        fprintf(fptr, 'MSE para o conjunto de validacao: %6.5f \n',desempenho.vperf(length(desempenho.vperf)));
        fprintf(fptr, 'MSE para o conjunto de teste: %6.5f \n',desempenhoTeste);
        fprintf(fptr, '\n');
        fprintf(fptr, 'Area sob a curva roc \n');
        fprintf(fptr, 'Neuronio da classe compradora: %1.3f \n', AUC_neuronio_Buy);
        fprintf(fptr, 'Neuronio da classe nao compradora: %1.3f \n', AUC_neuronio_dontBuy);
        fprintf(fptr, '\n');
        fprintf(fptr, 'Matriz de Confusao \n');
        fprintf(fptr, '                                                      Classes Verdadeiras \n\n', matrizConfusao(1,1), matrizConfusao(1,2));
        fprintf(fptr, '                                             Classe Compradora \t Classe nao compradora \n', matrizConfusao(1,1), matrizConfusao(1,2));
        fprintf(fptr, 'Classificacao       Classe Compradora             %4d          \t     %4d \n', matrizConfusao(1,1), matrizConfusao(1,2));
        fprintf(fptr, '                    Classe nao compradora         %4d          \t     %4d \n', matrizConfusao(2,1), matrizConfusao(2,2));
        fprintf(fptr, '\n');
        fprintf(fptr, 'Medidas extraidas da matriz de confusao \n');
        fprintf(fptr, 'fp Rate: %1.2f \n', fpRate);
        fprintf(fptr, 'tp Rate: %1.2f \n', tpRate);
        fprintf(fptr, 'Precision: %1.2f \n', precision);
        fprintf(fptr, 'Recall: %1.2f \n', recall);
        fprintf(fptr, 'Accuracy: %1.2f \n', accuracy);
        fprintf(fptr, 'F-measure: %1.2f \n', FMeasure);

        saveas(plot_buy, [OutFolderName,'/ROC Curve BUY.png']);
        saveas(plot_dontBuy, [OutFolderName,'/ROC Curve DontBUY.png']);

        save([OutFolderName,'/desempenho.mat'], 'desempenho');
        save([OutFolderName,'/matlab.mat']);

        fclose(fptr);


    end

end